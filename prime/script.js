function testPrime(n) {

  if (n === 1) {
    return false;
  } else if(n === 2) {
    return true;
  } else {
    for(var x = 2; x < n; x++) {
      if(n % x === 0) {
        return false;
      }
    }
    return true;  
  }
}

function check() {
    let text = document.getElementById("number").value
    let output = document.getElementById("output")
    console.log(testPrime(Number(text)))
    if(testPrime(Number(text)) === true) {
        output.innerHTML = "Prime";
    } else {
        output.innerHTML= "Not Prime";
    }
}
